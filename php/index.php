<?php 
$arrFSName = [];
$arrFirstName = [];
$arrSecondName = [];
$newAnimals = [];
$keys = [];
$arrResult = [];
$arrCountryAnimals = [
	'Afrika'=>['Mammuthus columbi', 'Tragelaphus', 'Hippopotamus', 'Syncerus caffer', 'Giraffa'],
	'Australia'=>['Dingo', 'Macropus', 'Phascolarctos cinereus', 'Myrmecobius fasciatus', 'Camelus'],
	'Asia'=>['Rhinocerotidae', 'Saiga tatarica', 'Rupicapra rupicapra', 'Cuon alpinus', 'Javanicus'],
	'Antarctica'=>['Hydrurga', 'Lobodon carcinophagus', 'Arctocephalus', 'gazella', 'leptonyx'],
	'Europe'=>['Sus scrofa', 'Ovibos moschatus', 'Lepus europaeus', 'Sciurus', 'Ursidae']
];

$keys = array_keys($arrCountryAnimals);

foreach ($arrCountryAnimals as $country => $animals){
	foreach ($animals as $animal){
		if(str_word_count($animal) == 2){
			$arrFSName = explode(' ', $animal);
			$arrFirstName[]=$arrFSName[0];
			$arrSecondName[]=$arrFSName[1];
		} 
	}
}

shuffle($arrFirstName);
shuffle($arrSecondName);
for ($i=0; $i<count($arrFirstName); $i++){
	$newAnimals[]=$arrFirstName[$i].' '.$arrSecondName[$i];
}

for ($i=0; $i<count($keys); $i++){
	$arrResult[$keys[$i]]=[];
	for($j=0; $j<count($arrCountryAnimals[$keys[$i]]); $j++){
		for($g=0; $g<count($arrFirstName); $g++){
			if(strpos($arrCountryAnimals[$keys[$i]][$j], $arrFirstName[$g])!==false){
				$arrResult[$keys[$i]][] = $newAnimals[$g];
			}
		}
	}
}

foreach ($arrResult as $country => $animals){
	echo "<h2>$country</h2>";
	echo implode(', ', $animals).'.';
}
?>